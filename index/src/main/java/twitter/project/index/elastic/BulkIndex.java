package twitter.project.index.elastic;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import twitter.project.index.database.Connector;

public class BulkIndex {
	public static TransportClient esClient;

	@SuppressWarnings({ "resource" })
	public static void init(String host, String port) {

		try {

//			Settings settings = Settings.builder()
//				    .put("shield.user", "elastic:xU2Iw6phVO2FyVYvvXgG") // your shield username and password
//				    .build();

			esClient = new PreBuiltTransportClient(Settings.EMPTY)
					.addTransportAddress(new TransportAddress(InetAddress.getByName(host), Integer.valueOf(port)));

//			 byte[] encodedBytes = Base64.getEncoder().encode("elastic:xU2Iw6phVO2FyVYvvXgG".getBytes());
//			esClient.threadPool().getThreadContext().putHeader("Authorization", "Basic "+new String(encodedBytes));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void close() {

		esClient.close();

	}

	public static void index(String dbPath) {
		int l = 1000;
		List<String> tweetKeys = new ArrayList<String>();
		List<String> userKeys = new ArrayList<String>();
		Map<String, String> userDic = new HashMap<String, String>();
		Map<String, String> tweetDic = new HashMap<String, String>();
		userDic = Connector.selectAll("dbUSjson", dbPath, l / 2);
		tweetDic = Connector.selectAll("dbTWjson", dbPath, l / 2);

		BulkProcessor bulkProcessor = BulkProcessor.builder(esClient, new BulkProcessor.Listener() {
			public void beforeBulk(long executionId, BulkRequest request) {
			}

			public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
				if (response.hasFailures()) {
					System.out.println("[Error]: Something went wrong on: " + response.buildFailureMessage());
				} else {
					System.out.println("[Log]: items were successfully indexed");
				}
			}

			public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
			}
		}).setBulkActions(l).setBulkSize(new ByteSizeValue(10, ByteSizeUnit.MB))
				.setFlushInterval(TimeValue.timeValueSeconds(1)).setConcurrentRequests(1)
				.setBackoffPolicy(BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)).build();
		int sum = userDic.size() + tweetDic.size();
		System.out.println("[Log]: " + sum + " items are in the process of indexing");
		for (Map.Entry<String, String> entry : userDic.entrySet()) {
			String key = entry.getKey();
			Object val = entry.getValue();
			bulkProcessor.add(new IndexRequest("user", "json", key).source(val, XContentType.JSON));
			userKeys.add(key);
		}
		for (Map.Entry<String, String> entry : tweetDic.entrySet()) {
			String key = entry.getKey();
			Object val = entry.getValue();
			bulkProcessor.add(new IndexRequest("tweet", "json", key).source(val, XContentType.JSON));
			tweetKeys.add(key);
		}
		bulkProcessor.flush();
		bulkProcessor.close();
//		for(String key: tweetKeys) {
//			Connector.
//		}

	}

}
