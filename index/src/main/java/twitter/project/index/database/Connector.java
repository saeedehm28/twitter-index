package twitter.project.index.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Connector {

	public static Connection init(String dbPath) throws SQLException {
		Connection conn = null;
		try {
			// db parameters
			String url = "jdbc:sqlite:" + dbPath;
			// create a connection to the database
			conn = DriverManager.getConnection(url);

			System.out.println("Connection to SQLite has been established.");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		if (conn != null) {
			return conn;
		} else
			return null;

	}

	public static void delete(int id, String dbPath, String table) {
		String sql = "DELETE FROM " + table + " WHERE id = ?";

		try (Connection conn = Connector.init(dbPath); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			// set the corresponding param
			pstmt.setInt(1, id);
			// execute the delete statement
			pstmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static Map<String, String> selectAll(String table, String dbPath, int limit) {
		String sql = "SELECT * FROM " + table + " Limit " + limit;
		Map<String, String> res = new HashMap<String, String>();

		try (Connection conn = Connector.init(dbPath);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			if(table =="dbUSjson") {
				while (rs.next()) {
					res.put(rs.getString("userid"), rs.getString("dbjson"));
				}
			}
			if(table =="dbTWjson") {
				while (rs.next()) {
					res.put(rs.getString("postid"), rs.getString("dbjson"));
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

}
